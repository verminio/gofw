package tracing

import (
	"reflect"
	"runtime"
	"time"
)

const (
	//DEBUG Trace level
	DEBUG Severity = iota
	//INFO Trace level
	INFO Severity = iota
	//ERROR Trace level
	ERROR Severity = iota
	//CUSTOM Trace level
	CUSTOM Severity = iota
)

//Severity of the Trace
type Severity int

//Trace message
type Trace struct {
	Start    time.Time
	End      time.Time
	Source   string
	Message  string
	Data     interface{}
	Severity Severity
}

//TraceList collection
type TraceList struct {
	ID     string
	Traces []Trace
}

//Manager for traces
type Manager interface {
	NewTrace(message string, data interface{}, severity Severity) Trace
	Flush(l *TraceList, e error)
	Push(t Trace, e error)
	Debug(message string, data interface{}) Trace
	Info(message string, data interface{}) Trace
	Error(message string, data interface{}) Trace
}

//Append trace to list
func (l *TraceList) Append(t Trace) {
	t.End = time.Now()
	l.Traces = append(l.Traces, t)
}

//NewList of traces (TraceList)
func NewList(id string) *TraceList {
	return &TraceList{
		ID: id,
	}
}

//GetCallerName for function
func GetCallerName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}
