package std

import (
	"log"
	"time"

	"runtime"

	"strconv"

	"gitlab.com/verminio/gofw/tracing"
)

//Manager for standard logging output
type Manager struct {
	logger   *log.Logger
	severity tracing.Severity
}

//New Manager instance
func New(logger *log.Logger, s tracing.Severity) *Manager {
	return &Manager{
		logger:   logger,
		severity: s,
	}
}

//NewTrace instance
func (m *Manager) NewTrace(message string, data interface{}, severity tracing.Severity) tracing.Trace {
	_, file, line, _ := runtime.Caller(1)
	source := file + strconv.Itoa(line)

	return tracing.Trace{
		Start:    time.Now(),
		Source:   source,
		Message:  message,
		Severity: severity,
	}
}

func (m *Manager) newTrace(message string, data interface{}, severity tracing.Severity) tracing.Trace {
	_, file, line, _ := runtime.Caller(2)
	source := file + ":" + strconv.Itoa(line)

	return tracing.Trace{
		Start:    time.Now(),
		Source:   source,
		Message:  message,
		Severity: severity,
	}
}

//Flush all trace messages
func (m *Manager) Flush(l *tracing.TraceList, e error) {
	for _, v := range l.Traces {
		m.write(l.ID, v, e)
	}
}

//Push single trace message
func (m *Manager) Push(t tracing.Trace, e error) {
	t.End = time.Now()
	m.write("", t, e)
}

//Debug trace instance
func (m *Manager) Debug(message string, data interface{}) tracing.Trace {
	return m.newTrace(message, data, tracing.DEBUG)
}

//Info trace instance
func (m *Manager) Info(message string, data interface{}) tracing.Trace {
	return m.newTrace(message, data, tracing.INFO)
}

//Error trace instance
func (m *Manager) Error(message string, data interface{}) tracing.Trace {
	return m.newTrace(message, data, tracing.ERROR)
}

func (m *Manager) write(id string, t tracing.Trace, e error) {
	duration := t.End.Sub(t.Start)
	if t.Severity >= m.severity || e != nil {
		m.logger.Printf("%s | %s | %s | %s | %dms", id, t.Source, t.Message, t.Start.Format(time.RFC3339), duration.Nanoseconds()/int64(time.Millisecond))
	}
}
