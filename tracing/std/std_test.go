package std

import (
	"log"
	"os"
	"testing"

	"gitlab.com/verminio/gofw/tracing"
)

func TestNew(t *testing.T) {
	var m tracing.Manager

	l := &tracing.TraceList{
		ID: "test",
	}

	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile)

	m = New(logger, tracing.INFO)
	defer m.Flush(l, nil)

	tr := m.Info("Testing Manager Creation", nil)

	l.Append(tr)

	if len(l.Traces) != 1 {
		t.Error("Wrong number of traces: 1")
	}

	tr2 := m.Info("Testing Manager Creation 2", nil)
	l.Append(tr2)

	if len(l.Traces) != 2 {
		t.Error("Wrong number of traces: 2")
	}
}
