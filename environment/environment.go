package environment

import (
	"os"
	"strconv"
)

//GetStringOrDefault returns the string value for an environment variable or a default value
func GetStringOrDefault(v string, d string) (res string) {
	res = os.Getenv(v)

	if res == "" {
		res = d
	}

	return res
}

//GetIntOrDefault returns the int value for an environment variable or a default value
func GetIntOrDefault(v string, d int) (res int) {
	res, err := strconv.Atoi(os.Getenv(v))
	if err != nil {
		res = d
	}

	return
}
