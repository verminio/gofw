package environment

//Context represents a generic Context
type Context interface {
	GetProperty(key string) interface{}
}
