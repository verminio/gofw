package std

import (
	"log"
	"os"

	"gitlab.com/verminio/gofw/metric"
)

//ConsoleHandler for writing metrics to std output
type ConsoleHandler struct {
	logger *log.Logger
}

//Dispatch the metrics
func (c *ConsoleHandler) Dispatch(data *metric.Collection) {
	for _, m := range data.Metrics {
		c.logger.Println(m.CSV())
	}
}

//New ConsoleHandler instance
func New() *ConsoleHandler {
	return &ConsoleHandler{
		logger: log.New(os.Stdout, "", log.Ldate|log.Ltime|log.Lshortfile),
	}
}
