package metric

import (
	"testing"
	"time"

	"gitlab.com/verminio/gofw/gen"
)

func TestNew(t *testing.T) {
	id := gen.RandomString(5)
	c := New(id)

	if c.TrackingID != id {
		t.Error("Collection created with wrong id, expected ", id, " got ", c.TrackingID)
	}

	if len(c.Metrics) != 0 {
		t.Error("Collection created with wrong number of metrics, expected 0, got ", len(c.Metrics))
	}
}

func TestStart(t *testing.T) {
	id := gen.RandomString(5)
	c := New(id)

	c.Start()

	if len(c.Metrics) != 1 {
		t.Error("Wrong number of metrics, expected 1, got ", len(c.Metrics))
	}

	m := c.Metrics[0]

	if m.TrackingID != id {
		t.Error("Wrong TrackingID, expected ", id, " got ", m.TrackingID)
	}
	if m.Type != "START" {
		t.Error("Wrong metric type, expected START, got ", m.Type)
	}
	if m.Function != "gitlab.com/verminio/gofw/metric.TestStart" {
		t.Error("Wrong metric function, expected gitlab.com/verminio/gofw/metric.TestStart, got ", m.Function)
	}
	if m.Sequence != 1 {
		t.Error("Wrong metric sequence number, expected 1, got ", m.Sequence)
	}
}

func TestEnd(t *testing.T) {
	id := gen.RandomString(5)
	c := New(id)

	c.Start()
	time.Sleep(1 * time.Second)
	c.End()

	if len(c.Metrics) != 2 {
		t.Error("Wrong number of metrics, expected 2, got ", len(c.Metrics))
	}

	m := c.Metrics[1]

	if m.TrackingID != id {
		t.Error("Wrong TrackingID, expected ", id, " got ", m.TrackingID)
	}

	if m.Type != "END" {
		t.Error("Wrong metric type, expected END, got ", m.Type)
	}

	if m.Function != "gitlab.com/verminio/gofw/metric.TestEnd" {
		t.Error("Wrong metric function, expected gitlab.com/verminio/gofw/metric.TestEnd, got ", m.Function)
	}

	if m.Sequence != 2 {
		t.Error("Wrong metric sequence number, expected 2, got ", m.Sequence)
	}

	if !(m.Timestamp > c.Metrics[0].Timestamp) {
		t.Error("End Timestamp expected to be after Start Timestamp.")
	}
}
