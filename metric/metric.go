package metric

import (
	"os"
	"runtime"
	"strconv"
	"time"

	"gitlab.com/verminio/gofw/gen"
)

const (
	start = "START"
	end   = "END"
)

type metric struct {
	TrackingID string
	Sequence   int
	Host       string
	Function   string
	Type       string
	Timestamp  int64
}

//Collection of metrics
type Collection struct {
	TrackingID string
	Metrics    []metric
}

//Handler for metric processing
type Handler interface {
	Dispatch(data *Collection)
}

func (c *Collection) next() int {
	return len(c.Metrics) + 1
}

func (m *metric) CSV() string {
	return m.TrackingID + "," + strconv.Itoa(m.Sequence) + "," + m.Host + "," + m.Function + "," + m.Type + "," + strconv.Itoa(int(m.Timestamp))
}

func newMetric(id string, t string, seq int) metric {
	hostname, _ := os.Hostname()
	function := getCallerName(3)
	return metric{
		TrackingID: id,
		Sequence:   seq,
		Host:       hostname,
		Function:   function,
		Type:       t,
		Timestamp:  time.Now().UnixNano(),
	}
}

func getCallerName(skip int) string {
	pc, _, _, _ := runtime.Caller(skip)
	return runtime.FuncForPC(pc).Name()
}

//New metric collection
func New(id string) *Collection {
	if id == "" {
		id = gen.RandomString(5) + strconv.Itoa(int(time.Now().UnixNano()))
	}
	return &Collection{
		TrackingID: id,
	}
}

//Start Metric
func (c *Collection) Start() {
	m := newMetric(c.TrackingID, start, c.next())
	c.Metrics = append(c.Metrics, m)
}

//End Metric
func (c *Collection) End() {
	m := newMetric(c.TrackingID, end, c.next())
	c.Metrics = append(c.Metrics, m)
}

//Custom Metric
func (c *Collection) Custom(t string) {
	m := newMetric(c.TrackingID, t, c.next())
	c.Metrics = append(c.Metrics, m)
}
