package web

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
	"strings"
	"time"

	"github.com/golang/glog"

	"github.com/gorilla/mux"

	"strconv"

	"gitlab.com/verminio/gofw/data"
	"gitlab.com/verminio/gofw/err"
	"gitlab.com/verminio/gofw/gen"
)

//ContextHandler web handler which receives Application context
type ContextHandler func(r *RequestWrapper, ctx AppContext)

//ResponseHandler for handling responses
type ResponseHandler func(rw *RequestWrapper)

//RequestWrapper to group web request dependencies
type RequestWrapper struct {
	TrackingID   string
	Response     http.ResponseWriter
	Request      *http.Request
	Claims       map[string]interface{}
	Error        error
	Entity       interface{}
	SkipResponse bool
}

//GetMuxVar get variable from gorilla/mux
func (r *RequestWrapper) GetMuxVar(v string) interface{} {
	vars := mux.Vars(r.Request)
	return vars[v]
}

//GetInt value from gorilla/mux
func (r *RequestWrapper) GetInt(v string) (val int, e error) {
	s := r.GetString(v)

	if val, e = strconv.Atoi(s); e != nil {
		e = TypeParseError
	}

	return
}

//GetString value from gorilla/mux
func (r *RequestWrapper) GetString(v string) string {
	return r.GetMuxVar(v).(string)
}

//AppContext Application Context definition
type AppContext struct {
	/*DataSource data.Source
	Properties map[string]interface{}*/
	data.Context
}

/*
//GetProperty from Properties
func (c *AppContext) GetProperty(key string) interface{} {
	return c.Properties[key]
}

//GetString from Properties
func (c *AppContext) GetString(key string) string {
	return c.Properties[key].(string)
}*/

//Handle general function for routing requests and Middleware
func Handle(ctx AppContext, mw []Middleware, handler ContextHandler, rsp ResponseHandler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		requestID := time.Now().Format("20060102150405") + gen.RandomString(5)
		//TODO: leave this only for development
		w.Header().Add("Access-Control-Allow-Origin", r.Header.Get("Origin"))
		w.Header().Add("Access-Control-Allow-Methods", r.Header.Get("Access-Control-Request-Method"))
		w.Header().Add("Access-Control-Allow-Headers", r.Header.Get("Access-Control-Request-Headers"))

		rw := &RequestWrapper{
			TrackingID: requestID,
			Request:    r,
			Response:   w,
		}

		Next(rw, ctx, mw, handler)

		rsp(rw)
	})
}

//DefaultResponseHandler for managing requests
func DefaultResponseHandler(rw *RequestWrapper) {
	if rw.SkipResponse {
		return
	}

	checkEntity(rw)

	if rw.Error != nil {
		handleError(rw)
	} else {
		http.Error(rw.Response, "", http.StatusOK)
	}
	handleResponse(rw)
}

func checkEntity(rw *RequestWrapper) {
	if rw.Response == nil && rw.Error != nil {
		rw.Entity = GenericResponse{Message: rw.Error.Error()}
	}
}

func handleError(rw *RequestWrapper) {
	r := rw.Error.(err.Error)

	if r.HTTPStatus != 0 {
		http.Error(rw.Response, "", r.HTTPStatus)
		return
	}

	switch r {
	case err.DuplicateKeyError:
		http.Error(rw.Response, "", http.StatusConflict)
	case err.NowRowsUpdatedError:
		http.Error(rw.Response, "", http.StatusNotFound)
	case err.AccountDisabledError:
		http.Error(rw.Response, "", http.StatusPreconditionFailed)
	case err.AccountLockedError, TypeParseError:
		http.Error(rw.Response, "", http.StatusBadRequest)
	case err.BadSyntaxError:
		http.Error(rw.Response, "", http.StatusInternalServerError)
	default:
		glog.Errorln("Unprocessed Error: ", r)
		http.Error(rw.Response, "", http.StatusInternalServerError)
	}

	return
}

func handleResponse(rw *RequestWrapper) {
	var t string
	switch rw.Request.Method {
	case http.MethodPost:
		t = rw.Request.Header.Get("Content-Type")
		break
	case http.MethodPut:
		t = rw.Request.Header.Get("Content-Type")
		break
	default:
		t = rw.Request.Header.Get("Accept")
	}

	t = strings.Split(t, ",")[0]
	rw.Response.Header().Set("Content-Type", t)

	if rw.Entity != nil {
		switch t {
		case "application/json":
			encoder := json.NewEncoder(rw.Response)
			encoder.Encode(rw.Entity)
			break
		case "application/xml":
			encoder := xml.NewEncoder(rw.Response)
			encoder.Encode(rw.Entity)
			break
		}
	}
	return
}
