package web

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
)

//Link represents an HTML Link
type Link struct {
	Rel  string
	Href string
}

//Resource represents a RESTful resources
type Resource struct {
	Links []Link `json:",omitempty"`
}

//FieldError error message for fields
type FieldError struct {
	Field string
	Error string
}

//GenericResponse response for api requests like POST
type GenericResponse struct {
	Message string       `json:",omitempty"`
	Errors  []FieldError `json:",omitempty"`
}

//AddError adds an error to generic response
func (r *GenericResponse) AddError(f string, e error) {
	r.Errors = append(r.Errors, FieldError{Field: f, Error: e.Error()})
}

//AddLink adds a new link to a resources list
func (r *Resource) AddLink(rel string, href string) {
	r.Links = append(r.Links, Link{Rel: rel, Href: href})
}

// ParseEntity parses an interface based on requests content-type
func ParseEntity(r *http.Request, e interface{}) error {

	t := r.Header.Get("Content-Type")

	switch t {
	case "application/json":
		decoder := json.NewDecoder(r.Body)
		return decoder.Decode(e)
	case "application/xml":
		decoder := xml.NewDecoder(r.Body)
		return decoder.Decode(e)
	default:
		return NoDecoderFoundError.WithHTTPStatus(http.StatusInternalServerError)
	}

}
