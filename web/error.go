package web

import "gitlab.com/verminio/gofw/err"
import "net/http"

var (
	//TypeParseError error to return when a type cannot be converted
	TypeParseError = err.New("Field is of wrong type.")
	//ValidationError error to return when request has validation errors
	ValidationError = err.New("Request has validation errors.").WithHTTPStatus(http.StatusBadRequest)
	//NoDecoderFoundError error to return when there is no decoder for the request content-type
	NoDecoderFoundError = err.New("No decoder found for Content-Type.").WithHTTPStatus(http.StatusInternalServerError)
)
