package web

//Middleware definition
type Middleware func(r *RequestWrapper, ctx AppContext, mw []Middleware, handler ContextHandler)

//Next Get next middleware
func Next(r *RequestWrapper, ctx AppContext, mw []Middleware, handler ContextHandler) {
	if len(mw) == 0 {
		handler(r, ctx)
	} else {
		mw[0](r, ctx, mw[1:], handler)
	}
}
