package web

import (
	"net/http"

	"github.com/golang/glog"
	"github.com/gorilla/mux"
)

var routes []Route

//Route definition
type Route struct {
	Name        string
	Method      string
	Pattern     string
	Description string
	Handler     ContextHandler
	Middleware  []Middleware
}

//Controller definition
type Controller struct {
	Name        string
	Description string
	Routes      []Route
}

//Register routes
func Register(r []Route) {
	routes = append(routes, r...)
}

//NewRouter with default setup
func NewRouter(ctx AppContext) (r *mux.Router) {
	return NewRouterWithResponseHandler(ctx, DefaultResponseHandler)
}

//NewRouterWithResponseHandler with custom ResponseHandler
func NewRouterWithResponseHandler(ctx AppContext, rsp ResponseHandler) (r *mux.Router) {
	r = mux.NewRouter().StrictSlash(true)
	//TODO: leave this only for development
	addCORSRoute(r)
	for _, route := range routes {
		glog.Infoln("Adding Route: " + route.Name + " | " + route.Method + " | " + route.Pattern)

		r.Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(Handle(ctx, route.Middleware, route.Handler, DefaultResponseHandler))
	}

	return
}

func addCORSRoute(r *mux.Router) {
	r.Methods(http.MethodOptions).
		Name("CORS Handler").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			glog.Infoln("Received OPTIONS request")
			w.Header().Add("Access-Control-Allow-Origin", r.Header.Get("Origin"))
			w.Header().Add("Access-Control-Allow-Methods", r.Header.Get("Access-Control-Request-Method"))
			w.Header().Add("Access-Control-Allow-Headers", r.Header.Get("Access-Control-Request-Headers"))
			w.WriteHeader(http.StatusOK)
		})
}
