package middleware

import (
	"time"

	"github.com/golang/glog"
	"gitlab.com/verminio/gofw/web"
)

// LogMiddleware for request logging
func LogMiddleware(r *web.RequestWrapper, ctx web.AppContext, mw []web.Middleware, handler web.ContextHandler) {
	start := time.Now()
	web.Next(r, ctx, mw, handler)
	glog.Infoln(r.Request.Method, r.Request.RequestURI, time.Since(start))
}
