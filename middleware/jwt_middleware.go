package middleware

import (
	"net/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"gitlab.com/verminio/gofw/err"
	"gitlab.com/verminio/gofw/web"
)

var (
	//ErrUnauthorized unauthorized request
	ErrUnauthorized = err.New("Unauthorized request.")
	//ErrInvalidToken invalid JWT token
	ErrInvalidToken = err.New("Invalid token.")
	//ErrInvalidAlg invalid algorithm error
	ErrInvalidAlg = err.New("Token contains bad information.")
)

// JWTMiddleware for processing JWT in requests
func JWTMiddleware(r *web.RequestWrapper, ctx web.AppContext, mw []web.Middleware, handler web.ContextHandler) {
	token, e := request.ParseFromRequestWithClaims(r.Request, request.AuthorizationHeaderExtractor, make(jwt.MapClaims), func(t *jwt.Token) (interface{}, error) {
		str := ctx.Properties["signingString"].([]byte)
		return str, nil
	})

	if e != nil {
		r.Error = ErrUnauthorized.WithNative(e).WithHTTPStatus(http.StatusUnauthorized)
		return
	}

	method := ctx.Properties["signingMethod"].(jwt.SigningMethod)

	if method.Alg() != token.Header["alg"] {
		r.Error = ErrInvalidAlg.WithHTTPStatus(http.StatusUnauthorized)
		return
	}

	if !token.Valid {
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		r.Claims = claims
	} else {
		r.Error = ErrInvalidToken.WithHTTPStatus(http.StatusBadRequest)
		return
	}

	web.Next(r, ctx, mw, handler)
}
