package err

var (
	//NowRowsUpdatedError for SQL
	NowRowsUpdatedError = New("Not found.")
	//AccountLockedError for user access
	AccountLockedError = New("Account is locked.")
	//AccountDisabledError when account hasn't been activated
	AccountDisabledError = New("Account hasn't been activated.")
	//DuplicateKeyError for duplicate data
	DuplicateKeyError = New("Duplicate key.")
	//BadSyntaxError for bad SQL statements
	BadSyntaxError = New("Bad query syntax.")
	//ConnectionRefusedError when unable to connect to backend
	ConnectionRefusedError = New("Backend refused the connection.")
)
