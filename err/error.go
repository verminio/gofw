package err

//Error definition
type Error struct {
	Code       string
	Message    string
	Native     error
	HTTPStatus int
}

//Resolver interface definition for error resolvers
type Resolver interface {
	ResolveString(code string) error
	ResolveNative(native error) error
	TranslateNative(native error) error
}

//type Error string

func (e Error) Error() string {
	return e.Message
}

func (e Error) WithNative(er error) Error {
	e.Native = er
	return e
}

func (e Error) WithHTTPStatus(status int) Error {
	e.HTTPStatus = status
	return e
}

func (e Error) WithCode(code string) Error {
	e.Code = code
	return e
}

//New error
func New(message string) Error {
	return Error{
		Message: message,
	}
}
