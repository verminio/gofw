package gen

import (
	"crypto/rand"
	"encoding/base64"
)

//RandomString with size
func RandomString(size int) string {
	b := make([]byte, size)
	_, err := rand.Read(b)

	if err != nil {
		panic(err)
	}

	return base64.URLEncoding.EncodeToString(b)
}
