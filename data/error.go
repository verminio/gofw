package data

import (
	"net"

	"gitlab.com/verminio/gofw/err"
)

//HandleNetError
func HandleNetError(e *net.OpError) error {
	switch e.Op {
	case "dial":
		return err.ConnectionRefusedError
	}

	return e
}
