package tx

import (
	"database/sql"
	"reflect"

	"github.com/golang/glog"
	"gitlab.com/verminio/gofw/data"
	"gitlab.com/verminio/gofw/err"
)

//DataSource for PostgreSQL database connection
type DataSource struct {
	Connection *sql.Tx
	Resolver   err.Resolver
}

//TestConnection to DataSource Backend
func (ds *DataSource) TestConnection() (err error) {
	return nil
}

//Get result from query execution
func (ds *DataSource) Get(query data.Query, result interface{}, params ...interface{}) (err error) {
	glog.Infoln(query.String.(string))

	stmt, err := ds.Connection.Prepare(query.String.(string))

	if err != nil {
		err = ds.HandleError(err)
		return
	}

	defer stmt.Close()

	rows, err := stmt.Query(params...)

	err = ds.HandleError(err)

	if err != nil {
		return
	}

	err = query.ResultMapper(rows, result)

	return
}

//Insert new record
func (ds *DataSource) Insert(query data.Query, params ...interface{}) (err error) {
	glog.Infoln(query.String.(string))

	stmt, err := ds.Connection.Prepare(query.String.(string))

	if err != nil {
		err = ds.HandleError(err)
		return
	}

	defer stmt.Close()

	_, err = stmt.Exec(params...)

	return ds.HandleError(err)
}

//Update existing record
func (ds *DataSource) Update(query data.Query, params ...interface{}) (rows int64, err error) {
	glog.Infoln(query.String.(string))

	stmt, err := ds.Connection.Prepare(query.String.(string))

	if err != nil {
		err = ds.HandleError(err)
		return
	}

	defer stmt.Close()

	r, err := stmt.Exec(params...)

	err = ds.HandleError(err)

	rows, err = r.RowsAffected()
	err = ds.HandleError(err)

	return
}

//HandleError database specific error
func (ds *DataSource) HandleError(e error) (err error) {
	if e == nil {
		return e
	}

	if err = ds.Resolver.ResolveNative(e); err != nil {
		return err
	}

	glog.Errorln(reflect.TypeOf(e).String(), e)

	return e
}

//GetConnection for DataSource
func (ds *DataSource) GetConnection() interface{} {
	return ds.Connection
}

//Commit the underlying transaction
func (ds *DataSource) Commit() error {
	return ds.Connection.Commit()
}

//Rollback the underlying transaction
func (ds *DataSource) Rollback() error {
	return ds.Connection.Rollback()
}

//New Transaction DataSource
func New(db *sql.DB, res err.Resolver) (ds *DataSource, e error) {
	tx, e := db.Begin()

	if e != nil {
		return
	}

	ds = &DataSource{
		Connection: tx,
		Resolver:   res,
	}

	return
}
