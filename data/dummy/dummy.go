package dummy

/*
import (
	"database/sql"
	"log"
	"net"
	"reflect"
	"time"

	"github.com/lib/pq"

	"gitlab.com/verminio/gofw/data"
)

//DataSource for PostgreSQL database connection
type DataSource struct {
	Data   interface{}
	Logger *log.Logger
}

//TestConnection to DataSource Backend
func (ds *DataSource) TestConnection() (err error) {
	return ds.Connection.Ping()
}

//GetResults from query execution
func (ds *DataSource) GetResults(query data.Query, result *interface{}, params ...interface{}) (err error) {

	start := time.Now()

	query.ResultMapper.Map(ds.Data, result)

	return
}

//GetSingleResult from query execution
func (ds *DataSource) GetSingleResult(query string, mapper data.Mapper, params ...interface{}) (res *interface{}, err error) {
	return
}

//Insert new record
func (ds *DataSource) Insert(query string, params ...interface{}) (err error) {
	return
}

//Update existing record
func (ds *DataSource) Update(query string, params ...interface{}) (rows int64, err error) {
	return
}

//HandleError database specific error
func (ds *DataSource) HandleError(e error, logger *log.Logger) (err error) {
	if e == nil {
		return e
	}

	switch e.(type) {
	case (*net.OpError):
		return data.HandleNetError(e.(*net.OpError), logger)
	case (*pq.Error):
		return handlePQError(e.(*pq.Error), logger)
	}

	printUncategorizedError(logger, reflect.TypeOf(e), "", e.Error())

	return e
}

func printUncategorizedError(logger *log.Logger, t reflect.Type, c string, msg string) {
	logger.Printf("| ERROR | Uncategorized Error | Type: %s | Code: %s | Message: %s", t, c, msg)
}

//SetLogger for data source
func (ds *DataSource) SetLogger(l *log.Logger) {

}

//GetConnection for DataSource
func (ds *DataSource) GetConnection() interface{} {
	return ds.connection
}

//New PostgreSQL DataSource
func New(driver string, url string) (ds *DataSource, err error) {
	db, err := sql.Open(driver, url)

	if err != nil {
		return
	}

	ds = &DataSource{
		Connection: db,
	}

	err = ds.TestConnection()

	return
}
*/
