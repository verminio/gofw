package data

//Context implementation of environment.Context with Source
type Context struct {
	DataSource Source
	Properties map[string]interface{}
}

//GetProperty from Properties
func (c *Context) GetProperty(key string) interface{} {
	return c.Properties[key]
}

//GetString from Properties
func (c *Context) GetString(key string) string {
	return c.Properties[key].(string)
}
