package data

import "gitlab.com/verminio/gofw/err"

var (
	//NowRowsUpdatedError for Data Source
	NowRowsUpdatedError = err.New("No matching rows found.")
)
