package data

//Source for generic data access
type Source interface {
	TestConnection() error
	Get(query Query, result interface{}, params ...interface{}) (err error)
	Insert(query Query, params ...interface{}) (err error)
	Update(query Query, params ...interface{}) (rows int64, err error)
	HandleError(e error) error
	GetConnection() interface{}
}

//Mapper result set mapping
type Mapper func(source interface{}, target interface{}) error

//Query object
type Query struct {
	String       interface{}
	ResultMapper Mapper
}
