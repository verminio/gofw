package pg

import (
	"database/sql"
	"testing"

	"gitlab.com/verminio/gofw/data"
)

const (
	dbURL = "postgres://gofwtest:gofwtest@pgserver"
)

type SQLMapper struct {
}

func (d *SQLMapper) Map(source interface{}, target *interface{}) {
	rows := source.(*sql.Rows)

}

func TestNewSuccess(t *testing.T) {
	_, err := New("postgres", dbURL)

	if err != nil {
		t.Error("Database connection failed.")
	}
}

func TestGet(t *testing.T) {
	ds, _ := New("postgres", dbURL)

	q := data.Query{
		String:       "SELECT 1 AS t",
		ResultMapper: &SQLMapper{},
	}
}
