package pg

import (
	"database/sql"
	"reflect"
	"time"

	"github.com/golang/glog"
	"gitlab.com/verminio/gofw/data"
	"gitlab.com/verminio/gofw/err"
)

//DataSource for PostgreSQL database connection
type DataSource struct {
	Connection *sql.DB
	Resolver   err.Resolver
}

//TestConnection to DataSource Backend
func (ds *DataSource) TestConnection() (err error) {
	return ds.Connection.Ping()
}

//Get result from query execution
func (ds *DataSource) Get(query data.Query, result interface{}, params ...interface{}) (err error) {
	start := time.Now()

	stmt, err := ds.Connection.Prepare(query.String.(string))

	if err != nil {
		err = ds.HandleError(err)
		return
	}

	defer stmt.Close()

	rows, err := stmt.Query(params...)

	err = ds.HandleError(err)

	if err != nil {
		return
	}

	defer rows.Close()
	err = query.ResultMapper(rows, result)

	glog.Infoln(query.String.(string), time.Since(start))
	return
}

//Insert new record
func (ds *DataSource) Insert(query data.Query, params ...interface{}) (err error) {
	start := time.Now()

	stmt, err := ds.Connection.Prepare(query.String.(string))

	if err != nil {
		err = ds.HandleError(err)
		return
	}

	defer stmt.Close()

	_, err = stmt.Exec(params...)

	glog.Infoln(query.String.(string), time.Since(start))
	return ds.HandleError(err)
}

//Update existing record
func (ds *DataSource) Update(query data.Query, params ...interface{}) (rows int64, err error) {
	start := time.Now()

	stmt, err := ds.Connection.Prepare(query.String.(string))

	if err != nil {
		err = ds.HandleError(err)
		return
	}

	defer stmt.Close()

	r, err := stmt.Exec(params...)

	err = ds.HandleError(err)

	rows, err = r.RowsAffected()
	err = ds.HandleError(err)

	glog.Infoln(query.String.(string), time.Since(start))
	return
}

//HandleError database specific error
func (ds *DataSource) HandleError(e error) (err error) {
	if e == nil {
		return e
	}

	if err = ds.Resolver.ResolveNative(e); err != nil {
		return err
	}

	glog.Errorln(reflect.TypeOf(e).String(), e)

	return e
}

//GetConnection for DataSource
func (ds *DataSource) GetConnection() interface{} {
	return ds.Connection
}

//New PostgreSQL DataSource
func New(driver string, url string, res err.Resolver) (ds *DataSource, err error) {
	db, err := sql.Open(driver, url)

	if err != nil {
		return
	}

	ds = &DataSource{
		Connection: db,
		Resolver:   res,
	}

	err = ds.TestConnection()

	return
}
