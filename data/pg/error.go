package pg

import (
	"net"

	"gitlab.com/verminio/gofw/data"
	"gitlab.com/verminio/gofw/err"

	"github.com/lib/pq"
)

//Resolver for SQL error messages
type Resolver struct {
}

//ResolveString error code
func (r *Resolver) ResolveString(code string) error {
	return nil
}

//ResolveNative error code
func (r *Resolver) ResolveNative(native error) error {
	switch native.(type) {
	case (*net.OpError):
		return data.HandleNetError(native.(*net.OpError))
	case (*pq.Error):
		e := native.(*pq.Error)
		switch e.Code {
		case pq.ErrorCode("23505"):
			return err.DuplicateKeyError
		case pq.ErrorCode("42804"):
			return err.BadSyntaxError
		case pq.ErrorCode("42703"):
			return err.BadSyntaxError
		}
	}

	return nil
}

//TranslateNative error code
func (r *Resolver) TranslateNative(native error) error {
	switch native.(type) {
	case (*net.OpError):
		return data.HandleNetError(native.(*net.OpError))
	case (*pq.Error):
		e := native.(*pq.Error)
		switch e.Code {
		case pq.ErrorCode("23505"):
			return err.DuplicateKeyError
		case pq.ErrorCode("42804"):
			return err.BadSyntaxError
		case pq.ErrorCode("42703"):
			return err.BadSyntaxError
		}
	}

	return nil
}

//handle postgresql error codes
func handlePQError(e *pq.Error) error {
	switch e.Code {
	case pq.ErrorCode("23505"):
		return err.DuplicateKeyError
	case pq.ErrorCode("42804"):
		return err.BadSyntaxError
	case pq.ErrorCode("42703"):
		return err.BadSyntaxError
	}

	return e
}
